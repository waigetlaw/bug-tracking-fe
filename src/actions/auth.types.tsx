import { ILoginResponse } from "../interfaces/ILoginResponse";

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_2FA_SUCCESS = 'LOGIN_2FA_SUCCESS';
export const LOGOUT = "LOGOUT";

export interface ILoginSuccessAction {
    readonly type: typeof LOGIN_SUCCESS
    payload: ILoginResponse;
}

export interface ILogin2faSuccessAction {
    readonly type: typeof LOGIN_2FA_SUCCESS
    payload: ILoginResponse
}

export interface ILogoutAction {
    readonly type: typeof LOGOUT
}

export type AuthActions =
    | ILoginSuccessAction
    | ILogin2faSuccessAction
    | ILogoutAction

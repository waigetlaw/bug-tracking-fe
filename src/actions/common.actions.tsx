import { IActionType } from "../interfaces/IActionType";
import { FAIL, IFailAction, IPendingAction, IResetErrorStatusOnPageChangeAction, PENDING, RESET_ERROR_STATUS_ON_PAGE_CHANGE } from "./common.types";

export const Pending = (actionType: IActionType): IPendingAction => {
    return {
        type: PENDING,
        payload: actionType
    }
}

export const Fail = (errorStatus: number): IFailAction => {
    return {
        type: FAIL,
        payload: errorStatus
    }
}

export const ResetErrorStatusOnPageChange = (): IResetErrorStatusOnPageChangeAction => {
    return {
        type: RESET_ERROR_STATUS_ON_PAGE_CHANGE
    }
}
export const CREATE_USER_SUCCESS = "CREATE_USER_SUCCESS";
export const UPDATE_PASSWORD_SUCCESS = "UPDATE_PASSWORD_SUCCESS";
export const ACTIVATE_2FA_SUCCESS = "ACTIVATE_2FA_SUCCESS";
export const DISABLE_2FA_SUCCESS = "DISABLE_2FA_SUCCESS";

export interface ICreateUserSuccessAction {
    readonly type: typeof CREATE_USER_SUCCESS
}

export interface IUpdatePasswordSuccessAction {
    readonly type: typeof UPDATE_PASSWORD_SUCCESS
}

export interface IActivate2FASuccessAction {
    readonly type: typeof ACTIVATE_2FA_SUCCESS
}

export interface IDisable2FASuccessAction {
    readonly type: typeof DISABLE_2FA_SUCCESS
}

export type UserActions = ICreateUserSuccessAction | IUpdatePasswordSuccessAction | IActivate2FASuccessAction | IDisable2FASuccessAction
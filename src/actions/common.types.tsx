import { IActionType } from "../interfaces/IActionType";

export const PENDING = "PENDING";
export const FAIL = "FAIL";
export const RESET_ERROR_STATUS_ON_PAGE_CHANGE = "RESET_ERROR_STATUS_ON_PAGE_CHANGE";

export interface IPendingAction {
    readonly type: typeof PENDING
    payload: IActionType
}

export interface IFailAction {
    readonly type: typeof FAIL,
    payload: number
}

export interface IResetErrorStatusOnPageChangeAction {
    readonly type: typeof RESET_ERROR_STATUS_ON_PAGE_CHANGE
}

export type CommonActions = IPendingAction | IFailAction | IResetErrorStatusOnPageChangeAction
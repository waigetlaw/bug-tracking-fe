import { IActionType } from "../interfaces/IActionType";
import { ICredentials } from "../interfaces/ICredentials";
import { ILoginResponse } from "../interfaces/ILoginResponse";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { instance } from "../utils/axios";
import { ILoginSuccessAction, LOGIN_SUCCESS, ILogoutAction, LOGOUT, ILogin2faSuccessAction, LOGIN_2FA_SUCCESS } from "./auth.types";
import { Fail, Pending } from "./common.actions";

export const login = (credentials: ICredentials) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.LOGIN));
    try {
        const result = await instance.post('/login', credentials);
        dispatch(LoginSuccess(result.data));
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
};

export const login2fa = (credentials: ICredentials) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.LOGIN2FA));
    try {
        const result = await instance.post('/login2fa', credentials);
        dispatch(Login2faSuccess(result.data));
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

const LoginSuccess = (response: ILoginResponse): ILoginSuccessAction => {
    return {
        type: LOGIN_SUCCESS,
        payload: response
    }
}

const Login2faSuccess = (response: ILoginResponse): ILogin2faSuccessAction => {
    return {
        type: LOGIN_2FA_SUCCESS,
        payload: response
    }
}

export const logout = () => async (dispatch: AppThunkDispatch) => {
    try {
        await instance.post('/logout');
    } finally {
        dispatch(Logout());
    }
}

const Logout = (): ILogoutAction => {
    return {
        type: LOGOUT
    };
};

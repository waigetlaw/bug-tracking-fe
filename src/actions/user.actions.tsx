import { IActionType } from "../interfaces/IActionType";
import { IPasscode, IPassword } from "../interfaces/ICredentials";
import { INewAccount } from "../interfaces/INewAccount";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { instance } from "../utils/axios";
import { Pending, Fail } from "./common.actions";
import { ACTIVATE_2FA_SUCCESS, CREATE_USER_SUCCESS, DISABLE_2FA_SUCCESS, IActivate2FASuccessAction, ICreateUserSuccessAction, IDisable2FASuccessAction, IUpdatePasswordSuccessAction, UPDATE_PASSWORD_SUCCESS } from "./user.types";

export const createUser = (newAccount: INewAccount) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.CREATE_NEW_USER));
    try {
        await instance.post('/users', newAccount);
        dispatch(CreateUserSuccess())
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

export const updatePassword = (password: IPassword) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.UPDATE_PASSWORD));
    try {
        await instance.put('/users', password);
        dispatch(UpdatePasswordSuccess())
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

export const activate2FA = (passcode: IPasscode) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.ACTIVATE_2FA))
    try {
        await instance.post('/confirm2fa', passcode);
        dispatch(Activate2faSuccess())
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

export const disable2FA = (passcode: IPasscode) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.DISABLE_2FA))
    try {
        await instance.post('/disable2fa', passcode);
        dispatch(Disable2faSuccess())
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

const CreateUserSuccess = (): ICreateUserSuccessAction => {
    return {
        type: CREATE_USER_SUCCESS
    }
}

const UpdatePasswordSuccess = (): IUpdatePasswordSuccessAction => {
    return {
        type: UPDATE_PASSWORD_SUCCESS
    }
}

const Activate2faSuccess = (): IActivate2FASuccessAction => {
    return {
        type: ACTIVATE_2FA_SUCCESS
    }
}

const Disable2faSuccess = (): IDisable2FASuccessAction => {
    return {
        type: DISABLE_2FA_SUCCESS
    }
}
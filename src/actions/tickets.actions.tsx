import { IActionType } from "../interfaces/IActionType";
import { IAddComment } from "../interfaces/IComment";
import { IEditTicket, INewTicket, ITicket } from "../interfaces/ITicket";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { instance } from "../utils/axios";
import { Fail, Pending } from "./common.actions";
import { ADD_COMMENT_SUCCESS, CREATE_TICKET_SUCCESS, EDIT_TICKET_SUCCESS, GET_TICKETS_SUCCESS, IAddCommentSuccessAction, ICreateTicketSuccessAction, IEditTicketSucessAction, IGetTicketsSuccessAction, IReopenTicketSuccessAction, REOPEN_TICKET_SUCCESS } from "./tickets.types";

export const getTickets = () => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.GET_TICKETS));
    try {
        const response = await instance.get<ITicket[]>('/tickets');
        dispatch(GetTicketsSuccess(response.data))
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

export const editTicket = ({ no, ...updateTicket }: IEditTicket) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.EDIT_TICKET));
    try {
        await instance.put(`/tickets/${no}`, updateTicket);
        dispatch(EditTicketSuccess())
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

export const createTicket = (ticket: INewTicket) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.CREATE_TICKET))
    try {
        await instance.post('/tickets', ticket);
        dispatch(CreateTicketSuccess())
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

export const addComment = (comment: IAddComment) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.ADD_COMMENT))
    try {
        const result = await instance.post<ITicket>('/comments', comment);
        dispatch(AddCommentSuccess(result.data))
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

export const reopenTicket = (ticketId: string) => async (dispatch: AppThunkDispatch) => {
    dispatch(Pending(IActionType.REOPEN_TICKET))
    try {
        const result = await instance.post<ITicket>(`/reopen/${ticketId}`);
        dispatch(ReopenTicketSuccess(result.data))
    } catch (e) {
        dispatch(Fail(e.response?.status))
    }
}

const GetTicketsSuccess = (tickets: ITicket[]): IGetTicketsSuccessAction =>
    ({ type: GET_TICKETS_SUCCESS, payload: tickets })

const EditTicketSuccess = (): IEditTicketSucessAction => {
    return {
        type: EDIT_TICKET_SUCCESS
    }
}

const CreateTicketSuccess = (): ICreateTicketSuccessAction => {
    return {
        type: CREATE_TICKET_SUCCESS
    }
}

const AddCommentSuccess = (updatedTicket: ITicket): IAddCommentSuccessAction => {
    return {
        type: ADD_COMMENT_SUCCESS,
        payload: updatedTicket
    }
}

const ReopenTicketSuccess = (updatedTicket: ITicket): IReopenTicketSuccessAction => {
    return {
        type: REOPEN_TICKET_SUCCESS,
        payload: updatedTicket
    }
}
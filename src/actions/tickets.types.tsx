import { ITicket } from "../interfaces/ITicket";

export const GET_TICKETS_SUCCESS = 'GET_TICKETS_SUCCESS';
export const EDIT_TICKET_SUCCESS = 'EDIT_TICKET_SUCCESS';
export const CREATE_TICKET_SUCCESS = 'CREATE_TICKET_SUCCESS';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const REOPEN_TICKET_SUCCESS = 'REOPEN_TICKET_SUCCESS';

export interface IGetTicketsSuccessAction {
    readonly type: typeof GET_TICKETS_SUCCESS
    payload: ITicket[];
}

export interface IEditTicketSucessAction {
    readonly type: typeof EDIT_TICKET_SUCCESS
}

export interface ICreateTicketSuccessAction {
    readonly type: typeof CREATE_TICKET_SUCCESS
}

export interface IAddCommentSuccessAction {
    readonly type: typeof ADD_COMMENT_SUCCESS
    payload: ITicket
}

export interface IReopenTicketSuccessAction {
    readonly type: typeof REOPEN_TICKET_SUCCESS
    payload: ITicket
}

export type TicketActions = IGetTicketsSuccessAction | IEditTicketSucessAction | ICreateTicketSuccessAction | IAddCommentSuccessAction | IReopenTicketSuccessAction
import { AuthActions, LOGIN_2FA_SUCCESS, LOGIN_SUCCESS, LOGOUT } from "../actions/auth.types";
import { CommonActions, FAIL, PENDING, RESET_ERROR_STATUS_ON_PAGE_CHANGE } from "../actions/common.types";
import { ADD_COMMENT_SUCCESS, CREATE_TICKET_SUCCESS, EDIT_TICKET_SUCCESS, GET_TICKETS_SUCCESS, REOPEN_TICKET_SUCCESS, TicketActions } from "../actions/tickets.types";
import { ACTIVATE_2FA_SUCCESS, CREATE_USER_SUCCESS, DISABLE_2FA_SUCCESS, UPDATE_PASSWORD_SUCCESS, UserActions } from "../actions/user.types";
import { IActionType } from "../interfaces/IActionType";
import { ITicket } from "../interfaces/ITicket";

export interface IAppState {
    isLoggedOn: boolean;
    user: {
        username: string;
        enabled2FA: boolean;
    } | null,
    tickets: ITicket[];
    processing: boolean;
    action: IAction;
}

export interface IAction {
    actionType: IActionType | null;
    errorStatus: number | null;
    success: boolean;
}

const initialState: IAppState = {
    isLoggedOn: false,
    user: null,
    tickets: [],
    processing: false,
    action: {
        actionType: null,
        errorStatus: null,
        success: false
    }
}



type Actions = CommonActions | AuthActions | TicketActions | UserActions;

const appReducer = (state: IAppState = initialState, action: Actions): IAppState => {

    function updateWithNewTicket(updatedTicket: ITicket) {
        return state.tickets.map((ticket) => ticket._id === updatedTicket._id ? updatedTicket : ticket);
    }

    switch (action.type) {
        case REOPEN_TICKET_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null }, tickets: updateWithNewTicket(action.payload) }
        case ADD_COMMENT_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null }, tickets: updateWithNewTicket(action.payload) }
        case CREATE_TICKET_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null } }
        case EDIT_TICKET_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null } }
        case DISABLE_2FA_SUCCESS:
            return { ...state, user: { username: state.user?.username || '', enabled2FA: false }, processing: false, action: { ...state.action, success: true, errorStatus: null } }
        case ACTIVATE_2FA_SUCCESS:
            return { ...state, user: { username: state.user?.username || '', enabled2FA: true }, processing: false, action: { ...state.action, success: true, errorStatus: null } }
        case UPDATE_PASSWORD_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null } }
        case GET_TICKETS_SUCCESS:
            return { ...state, tickets: action.payload.slice().sort((a, b) => a.no - b.no), processing: false, action: { ...state.action, success: true, errorStatus: null } }
        case CREATE_USER_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null } }
        case LOGIN_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null }, user: action.payload, isLoggedOn: !action.payload.enabled2FA }
        case LOGIN_2FA_SUCCESS:
            return { ...state, processing: false, action: { ...state.action, success: true, errorStatus: null }, user: action.payload, isLoggedOn: true }
        case PENDING:
            return { ...state, processing: true, action: { actionType: action.payload, errorStatus: null, success: false } }
        case FAIL:
            return { ...state, processing: false, action: { ...state.action, errorStatus: action.payload || 500 } }
        case RESET_ERROR_STATUS_ON_PAGE_CHANGE:
            return { ...state, processing: false, action: { actionType: null, errorStatus: null, success: false } }
        case LOGOUT:
            return initialState;
        default:
            return state;
    }
}

export default appReducer;

export interface IComment {
    timestamp: string;
    username: string;
    comment: string;
}

export interface IAddComment {
    ticketId: string;
    comment: string;
}
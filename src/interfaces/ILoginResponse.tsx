export interface ILoginResponse {
    username: string;
    enabled2FA: boolean;
}
export interface INewAccount {
    username: string;
    password: string;
    recaptcha: string;
}
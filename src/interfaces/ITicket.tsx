import { IComment } from "./IComment";

export interface ITicket {
    _id: string;
    type: EType;
    createdBy: string;
    title: string;
    no: number;
    description: string;
    timestamp: string;
    assigned: string;
    status: EStatus;
    priority: EPriority;
    comments: IComment[];
}

export interface INewTicket {
    type: EType;
    title: string;
    description: string;
    assigned: string;
    priority: EPriority;
}

export interface IEditTicket {
    type: EType;
    title: string;
    description: string;
    assigned: string;
    status: EStatus;
    no: number;
    priority: EPriority;
}

export enum EStatus { OPEN = "OPEN", RESOLVED = "RESOLVED", CLOSED = "CLOSED" }
export enum EPriority { LOW = "LOW", MEDIUM = "MEDIUM", HIGH = "HIGH" }
export enum EType { DEVELOPMENT = "DEVELOPMENT", TESTING = "TESTING", PRODUCTION = "PRODUCTION" }
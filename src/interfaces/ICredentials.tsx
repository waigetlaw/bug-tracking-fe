export interface ICredentials extends IPassword, IPasscode {
    username: string;
}

export interface IPassword {
    password: string;
}

export interface IPasscode {
    passcode?: string;
}
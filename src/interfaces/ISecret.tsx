export interface ISecret {
    secret: {
        ascii: string;
        hex: string;
        base32: string;
        otpauth_url: string;
    }
};
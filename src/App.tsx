import React from 'react';
import './App.css';
import { connect } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AuthRoute from './components/AuthRoute';
import Login from './components/Login';
import Home from './components/Home';
import { IAppState } from './reducers/app';
import NavBar from './components/NavBar';
import NewAccount from './components/NewAccount';
import Profile from './components/Profile';
import Footer from './components/Footer';

interface IAppProps {
  isLoggedOn: boolean
}

const App: React.FC<IAppProps> = props => {

  return (
    <div>

      <BrowserRouter>
        {
          props.isLoggedOn ? <NavBar /> : null
        }
        <div className="container">
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/new-account" component={NewAccount} />
            <AuthRoute path="/home" component={Home} />
            <AuthRoute path="/profile" component={Profile} />
            <AuthRoute path="/*" component={Home} />
          </Switch>
        </div>
      </BrowserRouter>
      <Footer />
    </div>
  );
}


export default connect((state: IAppState) => ({ isLoggedOn: state.isLoggedOn }))(App);

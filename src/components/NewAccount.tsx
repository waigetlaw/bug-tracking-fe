import { TextField, Button, Snackbar, SnackbarCloseReason, Dialog, DialogContent, DialogTitle, DialogActions } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { IAction, IAppState } from "../reducers/app";
import * as commonActions from "../actions/common.actions";
import * as userActions from "../actions/user.actions";
import { bindActionCreators } from "redux";
import ReCAPTCHA from "react-google-recaptcha";
import dotenv from "dotenv";

import "./NewAccount.css";
import { IActionType } from "../interfaces/IActionType";
import { Alert } from "./Alert";

dotenv.config();

interface INewAccountProps {
    dispatch: AppThunkDispatch;
    action: IAction;
}

const NewAccount: React.FC<INewAccountProps> = props => {

    const boundActionCreators = bindActionCreators({ ...commonActions, ...userActions }, props.dispatch)

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [success, setSuccess] = useState(false);
    const [reCaptchaValue, setReCaptchaValue] = useState<string | null>(null);

    const [error, setError] = useState('');

    const history = useHistory();

    useEffect(() => {
        if (props.action.actionType === IActionType.CREATE_NEW_USER && props.action.success) {
            setSuccess(true);
        }
        switch (props.action.errorStatus) {
            case 400:
                return setError("Incorrect values provided - please also complete the recaptcha");
            case 409:
                return setError("Username already taken - please choose another");
            case 500:
                return setError("Server error - try again later");
            default:
                return;
        }
    }, [props.action])

    function submitCreateAccount(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        setError('');
        if (password !== confirmPassword) {
            return setError("password and confirm password does not match")
        }
        if (!reCaptchaValue) {
            return setError("Please complete the reCAPTCHA")
        }
        boundActionCreators.createUser({ username, password, recaptcha: reCaptchaValue });
    }

    function back() {
        boundActionCreators.ResetErrorStatusOnPageChange()
        history.push('/login');
    }

    const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    };

    const handleConfirmPasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setConfirmPassword(event.target.value);
    };

    function handleSnackBarClose(event: React.SyntheticEvent<any, Event>, reason?: SnackbarCloseReason) {
        if (reason === 'clickaway') {
            return;
        }
        setError('');
    }

    function handleDialogClose() {
        boundActionCreators.ResetErrorStatusOnPageChange();
        history.push('/login');
    }

    const recaptchaRef = React.createRef<ReCAPTCHA>();

    function onRecaptchaChange(value: string | null) {
        setReCaptchaValue(value);
    }

    return (
        <div className="page">
            <h1>New Account</h1>

            <Button className="button" variant="contained" color="secondary" onClick={back}>Back</Button>

            <form className="form" onSubmit={submitCreateAccount} autoComplete="off">
                <TextField className="input" required id="standard-required" label="Username" defaultValue="" variant="outlined"
                    onChange={handleUsernameChange} />
                <TextField className="input" required id="standard-password-input" label="Password" type="password" variant="outlined"
                    onChange={handlePasswordChange} inputProps={{
                        minLength: 8,
                        pattern: "^.*[^a-zA-Z0-9\\s].*$",
                        onInvalid: (e: any) =>
                            e.target.setCustomValidity('password must contain at least 8 characters and at least 1 special character'),
                        onInput: (e: any) => e.target.setCustomValidity('')
                    }} />
                <TextField className="input" required id="standard-password-input" label="Confirm Password" type="password" variant="outlined"
                    onChange={handleConfirmPasswordChange} />
                <div className="g-captcha">
                    <ReCAPTCHA
                        ref={recaptchaRef}
                        sitekey={process.env.REACT_APP_RECAPTCHA_SITE_KEY || ''}
                        onChange={onRecaptchaChange}
                    />
                </div>
                <Button className="button" type="submit" variant="contained" color="primary" >Create</Button>
            </form>

            <Snackbar open={!!error} onClose={handleSnackBarClose}>
                <Alert onClose={handleSnackBarClose} severity="error">
                    {error}
                </Alert>
            </Snackbar>

            <Dialog open={success} fullWidth={true} maxWidth="xs" disableBackdropClick={true} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Account Created</DialogTitle>
                <DialogContent>You can now login with your new account.</DialogContent>

                <DialogActions>
                    <Button variant="outlined" onClick={handleDialogClose} color="primary">
                        Back to Login
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default connect((state: IAppState) => ({ action: state.action }))(NewAccount);

import { AppBar, Toolbar, IconButton, Typography, Menu, MenuItem, makeStyles, Divider } from "@material-ui/core";
import { AccountCircle, HomeSharp } from "@material-ui/icons";
import React, { useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { bindActionCreators } from "redux";
import { logout } from "../actions/auth.actions";
import { ResetErrorStatusOnPageChange } from "../actions/common.actions";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { IAppState } from "../reducers/app";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

interface INavBarProps {
    dispatch: AppThunkDispatch;
    username: string;
}

const NavBar: React.FC<INavBarProps> = props => {

    const classes = useStyles();
    const [anchorEl, setAnchorEl] = useState<EventTarget & HTMLButtonElement | null>(null);
    const open = Boolean(anchorEl);
    const history = useHistory();

    const boundActionCreators = bindActionCreators({ logout, ResetErrorStatusOnPageChange }, props.dispatch)

    const handleMenu = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const navToProfile = () => {
        boundActionCreators.ResetErrorStatusOnPageChange();
        history.push('/profile');
        setAnchorEl(null);
    }

    const handleLogout = () => {
        boundActionCreators.logout();
    }

    const goHome = () => {
        boundActionCreators.ResetErrorStatusOnPageChange();
        history.push('/home');
    }

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton
                    aria-label="home"
                    // aria-controls="menu-appbar"
                    aria-haspopup="false"
                    onClick={goHome}
                    color="inherit"
                    size="medium"
                >
                    <HomeSharp fontSize="large" />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    Bug Tracking Application
                </Typography>

                <Typography variant="h6">
                    {props.username}
                </Typography>
                <div>
                    <IconButton
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={handleMenu}
                        color="inherit"
                        disableRipple
                    >
                        <AccountCircle />
                    </IconButton>
                    <Menu
                        id="menu-appbar"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={open}
                        onClose={handleClose}
                    >
                        <MenuItem onClick={navToProfile}>Profile</MenuItem>
                        <Divider />
                        <MenuItem onClick={handleLogout}>Log Out</MenuItem>
                    </Menu>
                </div>
            </Toolbar>
        </AppBar>
    )
}

export default connect((state: IAppState) => ({ username: state.user?.username || '' }))(NavBar);

import { Button, Card, CardActions, CardContent, Chip, Divider, TextField, Typography } from "@material-ui/core";
import React, { useState } from "react";
import { connect } from "react-redux";
import { EStatus, ITicket } from "../interfaces/ITicket";
import { IAppState } from "../reducers/app";
import Comments from "./Comments";
import { reopenTicket } from "../actions/tickets.actions";

import "./ViewTicket.css"
import { AppThunkDispatch } from "../interfaces/Thunk";
import { bindActionCreators } from "redux";

interface IViewTicketProps extends ITicket {
    openEdit: () => void;
    close: () => void;
    username: string;
    dispatch: AppThunkDispatch;
}

const ViewTicket: React.FC<IViewTicketProps> = props => {

    const [openComments, setOpenComments] = useState(false);

    const handleOpenComments = () => setOpenComments(true);
    const handleCloseComments = () => setOpenComments(false);

    const boundActionCreators = bindActionCreators({ reopenTicket }, props.dispatch)

    function reopen() {
        boundActionCreators.reopenTicket(props._id);
    }

    return (
        <div className="card">
            <Card>
                <CardContent>
                    <Chip size="small" label={'# ' + props.no} color="primary" />
                    <Chip variant="outlined" size="small" className="float-right" label={props.status} color="primary" />
                    <Typography variant="h5" component="h2">{props.title}

                        {
                            props.status === EStatus.CLOSED &&
                            <Button style={{ float: "right", margin: "0.5rem 0 0.5rem 0.5rem" }} onClick={reopen}
                                variant="contained" size="small" color="secondary">Re-open</Button>
                        }
                    </Typography>

                    <Divider /><br />
                    <Typography variant="body2">Type: {props.type}</Typography>
                    <Typography variant="body2">Last Modified: {new Date(props.timestamp).toLocaleString()}</Typography>
                    <Typography variant="body2">Created By: {props.createdBy}</Typography>
                    <Typography variant="body2">Assigned: {props.assigned ? props.assigned : 'N/A'}</Typography>
                    <Typography variant="body2">Priority: {props.priority}</Typography>
                    <br />
                    <TextField className="input" id="standard-required" label="Description" variant="outlined" defaultValue={props.description}
                        multiline rows={8} fullWidth InputProps={{
                            readOnly: true,
                        }} />
                </CardContent>

                <CardActions>
                    <Button size="small" color="primary" variant="contained" onClick={handleOpenComments} style={{ marginRight: "auto" }}>Comments</Button>
                    {props.status !== EStatus.CLOSED && <Button disabled={(props.username !== props.createdBy && props.username !== props.assigned)}
                        size="small" variant="contained" onClick={props.openEdit} color="secondary">Edit</Button>}
                    <Button size="small" variant="outlined" onClick={props.close}>Close</Button>
                </CardActions>
                <Comments open={openComments} handleClose={handleCloseComments} status={props.status}
                    comments={props.comments.slice().sort((a, b) => new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime())} ticketId={props._id} />
            </Card>
        </div>
    )
}

export default connect((state: IAppState) => ({ username: state.user?.username || '' }))(ViewTicket);

import React from "react";
import { connect } from "react-redux";
import { Redirect, Route } from "react-router";
import { RouteComponentProps } from "react-router-dom";

interface IAuthRouteProps {
    isLoggedOn?: boolean;
    path: string;
    component?: React.ComponentType<RouteComponentProps<any>> | React.ComponentType<any>;
}


const AuthRoute: React.FC<IAuthRouteProps> = props => {
    if (!props.isLoggedOn) return <Redirect to="/login" />
    return <Route {...props} />;
};

export default connect((state: any) => ({ isLoggedOn: state.isLoggedOn }))(AuthRoute);

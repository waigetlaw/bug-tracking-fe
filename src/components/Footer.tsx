import React from "react";

var style: any = {
    backgroundColor: "#F8F8F8",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "10px 0",
    position: "fixed",
    left: "0",
    bottom: "0",
    width: "100%",
}

var phantom = {
    display: 'block',
    width: 'auto',
}

const Footer: React.FC = props => {
    return (
        <div>
            <div style={phantom} />
            <div style={style}>
                Bug Tracking Application &ndash; © 2020 Wai Get Law
            </div>
        </div>
    )
}

export default Footer
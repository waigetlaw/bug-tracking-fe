import { Card, CardContent, Chip, Dialog, Typography } from "@material-ui/core"
import { Color } from "@material-ui/lab/Alert";
import React, { useState } from "react"
import { connect } from "react-redux"
import { ITicket } from "../interfaces/ITicket"
import EditTicket from "./EditTicket";
import * as commonActions from "../actions/common.actions";

import "./TicketCard.css";
import ViewTicket from "./ViewTicket";
import { bindActionCreators } from "redux";
import { AppThunkDispatch } from "../interfaces/Thunk";

interface ITicketCardProps extends ITicket {
    dispatch: AppThunkDispatch;
    users: string[];
    setSeverity: (color: Color) => void;
    setSnackMsg: (msg: string) => void;
}

const TicketCard: React.FC<ITicketCardProps> = props => {

    const boundActionCreators = bindActionCreators(commonActions, props.dispatch);
    const [openView, setOpenView] = useState(false);
    const [openEdit, setOpenEdit] = useState(false);

    function handleOpenView() {
        setOpenView(true);
    }

    function handleCloseView() {
        boundActionCreators.ResetErrorStatusOnPageChange();
        setOpenView(false);
    }

    function handleOpenEdit() {
        setOpenEdit(true);
        setOpenView(false);
    }

    function handleCloseEdit() {
        boundActionCreators.ResetErrorStatusOnPageChange();
        setOpenEdit(false);
    }

    return (
        <div>
            <Card className="card-properties" onClick={handleOpenView}>
                <CardContent>
                    <Chip size="small" label={'# ' + props.no} color="primary" />
                    <Typography variant="h6" color="textPrimary">{props.title}</Typography><br />
                    <Typography variant="body2" color="textSecondary">Assigned to: {props.assigned ? props.assigned : 'N/A'}</Typography>
                </CardContent>
            </Card>

            <Dialog open={openView} onClose={handleCloseView} fullWidth disableBackdropClick>
                <ViewTicket {...props} openEdit={handleOpenEdit} close={handleCloseView} />
            </Dialog>

            <Dialog open={openEdit} onClose={handleCloseEdit} fullWidth disableBackdropClick>
                <EditTicket {...props} close={handleCloseEdit} setSeverity={props.setSeverity} setSnackMsg={props.setSnackMsg} users={props.users} />
            </Dialog>
        </div>
    )
}

export default connect()(TicketCard)

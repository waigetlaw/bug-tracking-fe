import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Snackbar, SnackbarCloseReason, TextField } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";
import { bindActionCreators } from "redux";
import * as authActions from "../actions/auth.actions";
import * as commonActions from "../actions/common.actions";
import { IActionType } from "../interfaces/IActionType";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { IAction, IAppState } from "../reducers/app";
import { Alert } from "./Alert";

import './Login.css';

interface ILoginProps {
    isLoggedOn: boolean;
    action: IAction;
    dispatch: AppThunkDispatch;
    user: { username: string, enabled2FA: boolean; } | null
}

const Login: React.FC<ILoginProps> = props => {

    const boundActionCreators = bindActionCreators({ ...authActions, ...commonActions }, props.dispatch)

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [passcode, setPasscode] = useState('');
    const [error, setError] = useState('');
    const [open2fa, setOpen2fa] = useState<boolean>(false);
    const history = useHistory();

    useEffect(() => {
        if (props.isLoggedOn) {
            boundActionCreators.ResetErrorStatusOnPageChange();
            history.push('home');
        }
    }, [history, props.isLoggedOn, props.user, boundActionCreators])

    useEffect(() => {
        if (props.action.actionType === IActionType.LOGIN && props.action.success) {
            setPasscode('');
            setOpen2fa(true);
        }
    }, [props.action.actionType, props.action.success])

    useEffect(() => {
        switch (props.action.errorStatus) {
            case 401:
                return setError("Wrong credentials");
            case 500:
                return setError("Server error - try again later");
            default:
                return;
        }
    }, [props.action])

    const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    };

    function handlePasscodeChange(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        const re = /^[0-9\b]+$/;
        if (event.target.value === '' || re.test(event.target.value)) {
            setPasscode(event.target.value.substr(0, 6));
        }
    }

    function submitLogin(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        boundActionCreators.login({ username, password });
    }

    function createNewAccount() {
        boundActionCreators.ResetErrorStatusOnPageChange();
        history.push("/new-account");
    }

    function handleSnackBarClose(event: React.SyntheticEvent<any, Event>, reason?: SnackbarCloseReason) {
        if (reason === 'clickaway') {
            return;
        }
        setError('');
    }

    function handle2faClose() {
        setOpen2fa(false);
    }

    function handle2faSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (passcode.length !== 6) return;
        boundActionCreators.login2fa({ username, password, passcode })
        setOpen2fa(false);
    }

    return (
        <div className="page">
            <h1>Bug Tracking Application</h1>

            <form className="form" onSubmit={submitLogin} autoComplete="off">
                <TextField className="input" required id="standard-required" label="Username" defaultValue="" variant="outlined"
                    onChange={handleUsernameChange} />
                <TextField className="input" required id="standard-password-input" label="Password" type="password" variant="outlined"
                    onChange={handlePasswordChange} />
                <Button className="button" type="submit" variant="contained" color="primary">Login</Button>
                <Button className="button" variant="contained" color="secondary" onClick={createNewAccount}>Create New Account</Button>
            </form>

            {/* 2fa pop up */}
            <Dialog open={open2fa} fullWidth={true} maxWidth="xs" disableBackdropClick={true} onClose={handle2faClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Two Factor Authentication</DialogTitle>
                <DialogContent>
                    <form onSubmit={handle2faSubmit} autoComplete="off">
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Passcode (6 digit)"
                            onChange={handlePasscodeChange}
                            value={passcode}
                        />
                        <DialogActions>
                            <Button variant="outlined" onClick={handle2faClose} color="primary">
                                Cancel
                            </Button>
                            <Button variant="contained" type="submit" disabled={passcode.length !== 6} color="primary">
                                Submit
                            </Button>
                        </DialogActions>
                    </form>
                </DialogContent>
            </Dialog>

            {/* error snackbar */}
            <Snackbar open={!!error} autoHideDuration={3000} onClose={handleSnackBarClose}>
                <Alert onClose={handleSnackBarClose} severity="error">
                    {error}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default connect((state: IAppState) => {
    return {
        isLoggedOn: state.isLoggedOn,
        action: state.action,
        user: state.user
    }
})(Login);

import { Button, CardActions, CardContent, Chip, Select, TextField, Typography } from "@material-ui/core";
import { Color } from "@material-ui/lab/Alert";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { EPriority, EStatus, EType, ITicket } from "../interfaces/ITicket";
import * as ticketActions from "../actions/tickets.actions";
import { bindActionCreators } from "redux";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { IAction, IAppState } from "../reducers/app";
import { IActionType } from "../interfaces/IActionType";

interface IEditTicketProps extends ITicket {
    close: () => void;
    dispatch: AppThunkDispatch;
    action: IAction;
    setSeverity: (color: Color) => void;
    setSnackMsg: (msg: string) => void;
    users: string[];
}

const EditTicket: React.FC<IEditTicketProps> = props => {

    const boundActionCreators = bindActionCreators(ticketActions, props.dispatch)
    const [type, setType] = useState<EType>(props.type);
    const [title, setTitle] = useState(props.title);
    const [status, setStatus] = useState<EStatus>(props.status);
    const [assigned, setAssigned] = useState(props.assigned ? props.assigned : '');
    const [priority, setPriority] = useState<EPriority>(props.priority);
    const [description, setDescription] = useState(props.description);

    useEffect(() => {
        if (props.action.actionType === IActionType.EDIT_TICKET) {
            if (props.action.success) {
                props.setSeverity("success");
                props.setSnackMsg("Ticket successfully updated");
                boundActionCreators.getTickets();
                props.close();
            }
            if (props.action.errorStatus) {
                props.setSeverity("error")
                props.setSnackMsg("Something went wrong");
            }
        }
    }, [props, boundActionCreators])

    function handleTypeChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setType(event.target.value as EType);
    }

    function handleTitleChange(event: React.ChangeEvent<HTMLInputElement>) {
        setTitle(event.target.value);
    }

    function handleDescriptionChange(event: React.ChangeEvent<HTMLInputElement>) {
        setDescription(event.target.value);
    }

    function handleStatusChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setStatus(event.target.value as EStatus);
    }

    function handlePriorityChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setPriority(event.target.value as EPriority);
    }

    function handleAssignedChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setAssigned(event.target.value as string);
    }



    function submitEditTicket() {
        boundActionCreators.editTicket({
            no: props.no,
            type,
            title,
            status,
            description,
            assigned,
            priority
        })
    }

    return (
        <div>
            <CardContent>
                <Chip size="small" label={'# ' + props.no} color="primary" />
                <Select className="float-right"
                    native
                    value={status}
                    onChange={handleStatusChange}
                >
                    {getAllowedStatusUpdates(props.status).map((status) =>
                        (
                            <option value={status} key={status}>{status}</option>
                        )
                    )}
                </Select>
                <br /><br />
                Type: <Select style={{ marginBottom: "1rem" }}
                    native
                    value={type}
                    onChange={handleTypeChange}
                >
                    {Object.values(EType).map(type =>
                        (
                            <option value={type} key={type}>{type}</option>
                        )
                    )}
                </Select>
                <TextField className="input" required id="standard-required" label="Title" variant="outlined" defaultValue={title}
                    onChange={handleTitleChange} autoComplete="off" inputProps={{ maxLength: 80 }} fullWidth />
                <br /><br />
                <Typography variant="body2">Last Modified: {new Date(props.timestamp).toLocaleString()}</Typography>
                <Typography variant="body2">Created By: {props.createdBy}</Typography>
                Assigned: <Select
                    native
                    value={assigned}
                    onChange={handleAssignedChange}
                >
                    <option value={""}>N/A</option>
                    {props.users.map((user) =>
                        (
                            <option value={user} key={user}>{user}</option>
                        )
                    )}
                </Select><br />

                Priority: <Select
                    native
                    value={priority}
                    onChange={handlePriorityChange}
                >
                    {Object.values(EPriority).map((priorityOption) =>
                        (
                            <option value={priorityOption} key={priorityOption}>{priorityOption}</option>
                        )
                    )}
                </Select>
                <br /><br />
                <TextField className="input" id="standard-required" label="Description" variant="outlined" defaultValue={description}
                    onChange={handleDescriptionChange} multiline rows={8} fullWidth inputProps={{ maxLength: 600 }} />
            </CardContent>
            <CardActions>
                <Button size="small" style={{ marginLeft: "auto" }} variant="contained" color="secondary" onClick={submitEditTicket}>Save</Button>
                <Button size="small" variant="outlined" onClick={props.close}>Cancel</Button>
            </CardActions>
        </div>
    )
}

function getAllowedStatusUpdates(currentStatus: EStatus) {
    switch (currentStatus) {
        case EStatus.OPEN: {
            return [EStatus.OPEN, EStatus.RESOLVED];
        }
        case EStatus.RESOLVED: {
            return [EStatus.RESOLVED, EStatus.CLOSED];
        }
        case EStatus.CLOSED: {
            return [EStatus.CLOSED]
        }
    }
}

export default connect((state: IAppState) => ({ action: state.action }))(EditTicket);

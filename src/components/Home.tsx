import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { AppThunkDispatch } from "../interfaces/Thunk";
import * as ticketActions from "../actions/tickets.actions";
import { IAction, IAppState } from "../reducers/app";
import { EPriority, EStatus, EType, ITicket } from "../interfaces/ITicket";
import TicketCard from "./TicketCard";

import './Home.css';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Select, Snackbar, SnackbarCloseReason, TextField } from "@material-ui/core";
import { instance } from "../utils/axios";
import { Alert } from "./Alert";
import { Color } from "@material-ui/lab/Alert";
import { IActionType } from "../interfaces/IActionType";

interface IHomeProps {
    dispatch: AppThunkDispatch;
    tickets: ITicket[];
    action: IAction;
}

interface ISortedTickets {
    [key: string]: {
        open: ITicket[];
        resolved: ITicket[];
        closed: ITicket[];
    }
}

const Home: React.FC<IHomeProps> = props => {

    const [sortedTickets, setSortedTickets] = useState<ISortedTickets>({});
    const boundActionCreators = bindActionCreators(ticketActions, props.dispatch)
    const [openCreate, setOpenCreate] = useState(false);
    const [viewType, setViewType] = useState<EType>(EType.DEVELOPMENT);
    const [type, setType] = useState<EType>(EType.DEVELOPMENT);
    const [title, setTitle] = useState('');
    const [assigned, setAssigned] = useState('');
    const [description, setDescription] = useState('');
    const [priority, setPriority] = useState<EPriority>(EPriority.LOW);
    const [users, setUsers] = useState<string[]>([]);
    const [snackMsg, setSnackMsg] = useState('');
    const [severity, setSeverity] = useState<Color>('error');

    useEffect(() => {
        boundActionCreators.getTickets();
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        getUsers();
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        if (props.action) {
            switch (props.action.actionType) {
                case (IActionType.CREATE_TICKET): {
                    if (props.action.success) {
                        setSeverity("success");
                        setSnackMsg("Ticket successfully created");
                        boundActionCreators.getTickets();
                        handleCreateClose();
                    }
                    if (props.action.errorStatus) {
                        if (props.action.errorStatus === 400) {
                            setSeverity("error")
                            setSnackMsg("Bad input - please make sure you have given a title");
                        } else {
                            setSeverity("error")
                            setSnackMsg("Something went wrong");
                        }
                    }
                    break;
                }
                case (IActionType.ADD_COMMENT): {
                    if (props.action.success) {
                        setSeverity("success");
                        setSnackMsg("Comment successfully added");
                    }
                    if (props.action.errorStatus) {
                        if (props.action.errorStatus === 400) {
                            setSeverity("error")
                            setSnackMsg("bad input - ticket is not found or closed");
                        } else {
                            setSeverity("error")
                            setSnackMsg("Something went wrong");
                        }
                    }
                    break;
                }
                case (IActionType.REOPEN_TICKET): {
                    if (props.action.success) {
                        setSeverity("success");
                        setSnackMsg("Ticket successfully re-opened");
                    }
                    if (props.action.errorStatus) {
                        setSeverity("error")
                        setSnackMsg("Something went wrong");
                    }
                    break;
                }
            }
        }
        // eslint-disable-next-line
    }, [props.action, boundActionCreators])

    async function getUsers() {
        try {
            const response = await instance.get<string[]>('/users');
            setUsers(response.data.slice().sort((a, b) => a.localeCompare(b)));
        } catch (e) {
            setSeverity("error")
            setSnackMsg("Error retrieving list of users from system")
        }
    }

    function handleViewTypeChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setViewType(event.target.value as EType);
        setType(event.target.value as EType); // make default new ticket to be same as viewing board type
    }

    function handleTypeChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setType(event.target.value as EType);
    }

    const handleCreateClose = () => {
        setOpenCreate(false) // close and reset form to defaults
        setTitle('')
        setAssigned('')
        setDescription('')
        setPriority(EPriority.LOW)
        setType(viewType)
    }

    function handleTitleChange(event: React.ChangeEvent<HTMLInputElement>) {
        setTitle(event.target.value);
    }

    function handleDescriptionChange(event: React.ChangeEvent<HTMLInputElement>) {
        setDescription(event.target.value);
    }

    function handleAssignedChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setAssigned(event.target.value as string);
    }

    function handlePriorityChange(event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
    }>) {
        setPriority(event.target.value as EPriority);
    }

    function handleSnackBarClose(event: React.SyntheticEvent<any, Event>, reason?: SnackbarCloseReason) {
        if (reason === 'clickaway') {
            return;
        }
        setSnackMsg('');
    }

    function handleCreateTicket() {
        boundActionCreators.createTicket({ type, title, assigned, description, priority });
    }

    useEffect(() => {
        const sorted = props.tickets.reduce((acc: ISortedTickets, ticket) => {
            if (!acc[ticket.type]) {
                acc[ticket.type] = { open: [], resolved: [], closed: [] };
            }
            switch (ticket.status) {
                case EStatus.OPEN:
                    acc[ticket.type].open.push(ticket)
                    break;
                case EStatus.RESOLVED:
                    acc[ticket.type].resolved.push(ticket)
                    break;
                case EStatus.CLOSED:
                    acc[ticket.type].closed.push(ticket)
                    break;
                default:
                    break;
            }
            return acc;
        }, {})
        setSortedTickets(sorted);
    }, [props.tickets])

    return (
        <div>
            <Button size="large" className="create-button" variant="contained" color="secondary" onClick={() => setOpenCreate(true)}>Create New Ticket</Button>

            <Select className="float-right"
                style={{ margin: "1rem" }}
                native
                value={viewType}
                onChange={handleViewTypeChange}
            >
                {Object.values(EType).map(type =>
                    (
                        <option value={type} key={type}>{type}</option>
                    )
                )}
            </Select>

            <hr />
            <h1 style={{ textAlign: "center" }}>{viewType}</h1>
            <hr />
            <div className="page">
                <div className="ticket-container">
                    <div className="ticket-column">
                        <h2 className="text-center">OPEN</h2>
                        {
                            sortedTickets[viewType]?.open.map((ticket) => {
                                return (
                                    <TicketCard key={ticket.no} {...ticket} users={users} setSeverity={setSeverity} setSnackMsg={setSnackMsg} />
                                )
                            })
                        }
                    </div>
                    <div className="ticket-column">
                        <h2 className="text-center">RESOLVED</h2>
                        {
                            sortedTickets[viewType]?.resolved.map((ticket) => {
                                return (
                                    <TicketCard key={ticket.no} {...ticket} users={users} setSeverity={setSeverity} setSnackMsg={setSnackMsg} />
                                )
                            })
                        }
                    </div>
                    <div className="ticket-column">
                        <h2 className="text-center">CLOSED</h2>
                        {
                            sortedTickets[viewType]?.closed.map((ticket) => {
                                return (
                                    <TicketCard key={ticket.no} {...ticket} users={users} setSeverity={setSeverity} setSnackMsg={setSnackMsg} />
                                )
                            })
                        }
                    </div>
                </div>
                <Dialog open={openCreate} onClose={handleCreateClose} fullWidth disableBackdropClick>
                    <DialogTitle id="alert-dialog-title">Create New Ticket</DialogTitle>

                    <DialogContent>
                        Type: <Select style={{ marginBottom: "1rem" }}
                            native
                            value={type}
                            onChange={handleTypeChange}
                        >
                            {Object.values(EType).map(type =>
                                (
                                    <option value={type} key={type}>{type}</option>
                                )
                            )}
                        </Select>
                        <TextField className="input" required id="standard-required" label="Title" variant="outlined" defaultValue={title}
                            onChange={handleTitleChange} autoComplete="off" inputProps={{ maxLength: 80 }} fullWidth />

                        Assigned: <Select
                            native
                            value={assigned}
                            onChange={handleAssignedChange}
                        >
                            <option value={""}>N/A</option>
                            {users.map((user) =>
                                (
                                    <option value={user} key={user}>{user}</option>
                                )
                            )}
                        </Select><br />

                        Priority: <Select
                            native
                            value={priority}
                            onChange={handlePriorityChange}
                        >
                            {Object.values(EPriority).map((priorityOption) =>
                                (
                                    <option value={priorityOption} key={priorityOption}>{priorityOption}</option>
                                )
                            )}
                        </Select>

                        <br /><br />
                        <TextField className="input" id="standard-required" label="Description" variant="outlined" defaultValue={description}
                            onChange={handleDescriptionChange} multiline rows={8} fullWidth inputProps={{ maxLength: 500 }} />
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="primary" onClick={handleCreateTicket}>Create</Button>
                        <Button onClick={handleCreateClose} variant="outlined" color="primary">Cancel</Button>
                    </DialogActions>
                </Dialog>
                <Snackbar open={!!snackMsg} onClose={handleSnackBarClose} autoHideDuration={3000}>
                    <Alert onClose={handleSnackBarClose} severity={severity}>
                        {snackMsg}
                    </Alert>
                </Snackbar>
            </div>
        </div>
    )
}

export default connect((state: IAppState) => ({ tickets: state.tickets, action: state.action }))(Home);

import { Button, Grid, IconButton, Snackbar, SnackbarCloseReason, TextField, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { IAction, IAppState } from "../reducers/app";
import * as userActions from "../actions/user.actions";
import { bindActionCreators } from "redux";
import { Alert } from "./Alert";
import { IActionType } from "../interfaces/IActionType";
import { Color } from "@material-ui/lab/Alert";
import QRCode from 'qrcode.react';
import { CancelTwoTone, CheckCircleTwoTone, FileCopyOutlined } from "@material-ui/icons";
import { instance } from "../utils/axios";
import { ISecret } from "../interfaces/ISecret";

import "./Profile.css";

interface IProfileProps {
    dispatch: AppThunkDispatch;
    action: IAction;
    enabled2FA: boolean | undefined;
}

const Profile: React.FC<IProfileProps> = props => {

    const boundActionCreators = bindActionCreators(userActions, props.dispatch)
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [passcode, setPasscode] = useState('');
    const [secret2fa, setSecret2fa] = useState<ISecret | null>(null);
    const [snackMsg, setSnackMsg] = useState('');
    const [severity, setSeverity] = useState<Color>('error');

    useEffect(() => {
        if (props.action.actionType === IActionType.UPDATE_PASSWORD) {
            if (props.action.success) {
                setSeverity("success")
                setSnackMsg("Password was successfully updated")
                setPassword("")
                setConfirmPassword("")
            } else if (props.action.errorStatus) {
                setSeverity("error")
                setSnackMsg("Something went wrong updating the password - Please try again")
            }
        }
        if (props.action.actionType === IActionType.ACTIVATE_2FA) {
            if (props.action.success) {
                setSeverity("success")
                setSnackMsg("2fa was enabled successfully")
                setSecret2fa(null)
                setPasscode('')
            } else if (props.action.errorStatus) {
                setSeverity("error")
                setSnackMsg("Wrong passcode - please try again")
            }
        }

        if (props.action.actionType === IActionType.DISABLE_2FA) {
            if (props.action.success) {
                setSeverity("success")
                setSnackMsg("2fa was disabled successfully")
                setSecret2fa(null)
                setPasscode('')
            } else if (props.action.errorStatus) {
                setSeverity("error")
                setSnackMsg("Wrong passcode - please try again")
            }
        }
    }, [props.action])

    const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    };

    const handleConfirmPasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setConfirmPassword(event.target.value);
    };

    function handlePasscodeChange(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        const re = /^[0-9\b]+$/;
        if (event.target.value === '' || re.test(event.target.value)) {
            setPasscode(event.target.value.substr(0, 6));
        }
    }

    function handleUpdatePassword(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault();
        if (password === confirmPassword) {
            boundActionCreators.updatePassword({ password });
        } else {
            setSeverity("error")
            setSnackMsg("password and confirm password does not match")
        }
    }

    function handleSnackBarClose(event: React.SyntheticEvent<any, Event>, reason?: SnackbarCloseReason) {
        if (reason === 'clickaway') {
            return;
        }
        setSnackMsg('');
    }

    async function handleEnable2FA() {
        try {
            const response = await instance.get<ISecret>('/get2fa');
            setSecret2fa(response.data);
        } catch (e) {
            setSeverity("error");
            setSnackMsg("Error retrieving 2FA secret - please try again later");
        }
    }

    function handleActivate2fa() {
        if (passcode.length === 6) {
            boundActionCreators.activate2FA({ passcode });
        } else {
            setSeverity("error");
            setSnackMsg("Please enter a passcode to confirm 2fa")
        }
    }

    function handleDisable2FA() {
        if (passcode.length === 6) {
            boundActionCreators.disable2FA({ passcode });
        } else {
            setSeverity("error");
            setSnackMsg("Please enter a valid passcode to disable 2FA")
        }
    }

    function copyToClipboard(text: string) {
        var dummy = document.createElement("textarea");
        // to avoid breaking orgain page when copying more words
        // cant copy when adding below this code
        // dummy.style.display = 'none'
        document.body.appendChild(dummy);
        //Be careful if you use texarea. setAttribute('value', value), which works with "input" does not work with "textarea". – Eduard
        dummy.value = text;
        dummy.select();
        document.execCommand("copy");
        document.body.removeChild(dummy);
    }

    function copySecret() {
        copyToClipboard(secret2fa?.secret.base32 || '')
        setSeverity("info")
        setSnackMsg("Copied!")
    }

    return (
        <div className="page">
            <Grid container alignContent="center">
                <Grid item xs>
                    <Typography gutterBottom variant="h4">Account</Typography>
                    <Typography color="textSecondary" variant="body1">
                        Change password
                </Typography>
                    <br />
                    <form className="form" onSubmit={handleUpdatePassword} id="change-password-form">
                        <TextField size="small" className="input" required id="standard-password-input" label="Password" type="password" variant="outlined"
                            value={password}
                            onChange={handlePasswordChange} inputProps={{
                                minLength: 8,
                                pattern: "^.*[^a-zA-Z0-9\\s].*$",
                                onInvalid: (e: any) =>
                                    e.target.setCustomValidity('password must contain at least 8 characters and at least 1 special character'),
                                onInput: (e: any) => e.target.setCustomValidity('')
                            }} />
                        <TextField size="small" className="input" required id="standard-password-input" label="Confirm Password" type="password" variant="outlined"
                            value={confirmPassword} onChange={handleConfirmPasswordChange} />
                    </form>
                    <Button className="password-button" size="small" form="change-password-form" type="submit" variant="outlined" color="primary">Update Password</Button>
                </Grid>
            </Grid>

            <Grid container alignContent="center">
                <Grid item xs>
                    <Typography gutterBottom variant="h5">Security</Typography>
                    <Typography color="textSecondary" variant="body1">
                        Two-Factor Authentication (2FA)
                    </Typography>
                    <br />
                    {
                        // has 2fa?
                        props.enabled2FA ?
                            <div>
                                <Typography variant="body1">
                                    <CheckCircleTwoTone className="success material-icons" /> <span>2FA is enabled</span>
                                </Typography>
                                <br />
                                <Typography color="textSecondary" variant="body1">To disable 2FA, please enter a valid passcode</Typography>
                                <br />
                                <TextField size="small" required id="standard-password-input" label="Passcode" variant="outlined"
                                    onChange={handlePasscodeChange} value={passcode} />
                                <Button variant="contained" color="primary" className="button-2fa" onClick={handleDisable2FA}>Disable 2FA</Button>
                            </div> :

                            // no 2fa
                            <div>
                                <Typography variant="body1">
                                    <CancelTwoTone color="error" className="material-icons" /> <span>2FA is disabled - generate 2FA code to enable</span>
                                </Typography>
                                <br />
                                <Button variant="contained" color="primary" onClick={handleEnable2FA}>Generate 2FA code</Button>

                                {/* got secret */}
                                {secret2fa &&
                                    <div className="secret-2fa" style={{ margin: "2rem 0" }}>
                                        <Typography color="textSecondary" variant="body1">Scan the QR code or manually type the secret to a 2FA app (e.g. Authy):</Typography>
                                        <br />
                                        <Typography>Secret: {secret2fa.secret.base32}<IconButton onClick={copySecret}><FileCopyOutlined /></IconButton></Typography>

                                        <br />
                                        <QRCode value={secret2fa.secret.otpauth_url} />
                                        <br /><br />
                                        <Typography color="textSecondary" variant="body1">Please enter the passcode and confirm to activate 2FA</Typography>
                                        <br />
                                        <TextField size="small" required id="standard-password-input" label="Passcode" variant="outlined"
                                            onChange={handlePasscodeChange} value={passcode} />
                                        <Button variant="contained" className="button-2fa" color="primary" onClick={handleActivate2fa}>Activate 2FA</Button>
                                    </div>
                                }
                            </div>
                    }
                </Grid>
            </Grid>

            <Snackbar open={!!snackMsg} autoHideDuration={3000} onClose={handleSnackBarClose}>
                <Alert onClose={handleSnackBarClose} severity={severity}>
                    {snackMsg}
                </Alert>
            </Snackbar>
        </div>
    )
}

export default connect((state: IAppState) => ({ action: state.action, enabled2FA: state.user?.enabled2FA }))(Profile);

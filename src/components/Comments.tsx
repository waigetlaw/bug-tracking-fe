import { Button, Card, CardActions, CardContent, Dialog, Divider, TextField, Typography } from "@material-ui/core";
import React, { useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { IComment } from "../interfaces/IComment";
import { addComment } from "../actions/tickets.actions";
import { AppThunkDispatch } from "../interfaces/Thunk";
import { EStatus } from "../interfaces/ITicket";

interface ICommentsProps {
    open: boolean;
    handleClose: () => void;
    comments: IComment[];
    ticketId: string;
    dispatch: AppThunkDispatch;
    status: EStatus;
}

const Comments: React.FC<ICommentsProps> = props => {

    const [comment, setComment] = useState("");

    const boundActionCreators = bindActionCreators({ addComment }, props.dispatch)

    function handleCommentChange(event: React.ChangeEvent<HTMLInputElement>) {
        setComment(event.target.value);
    }

    function addNewComment() {
        boundActionCreators.addComment({ ticketId: props.ticketId, comment })
    }

    return (
        <Dialog open={props.open} onClose={props.handleClose} fullWidth maxWidth="sm" disableBackdropClick>
            <Card>
                <CardContent>
                    <Typography variant="h5" component="h2">Comments</Typography>
                    <Divider /><br />
                    <div style={{ height: "20rem", overflowY: "auto" }}>
                        {
                            props.comments.map((comment, i) => (
                                <Card variant="outlined" style={{ margin: "0.5rem 0" }} key={i}>
                                    <CardContent>
                                        <Typography variant="body2">Timestamp: {new Date(comment.timestamp).toLocaleString()}</Typography>
                                        <Typography variant="body2">Username: {comment.username}</Typography>
                                        <Divider />
                                        <Typography variant="body2">{comment.comment}</Typography>
                                    </CardContent>
                                </Card>
                            ))
                        }
                    </div>
                    {
                        props.status !== EStatus.CLOSED &&
                        <div>
                            <TextField className="input" id="standard-required" label="New Comment" variant="outlined" defaultValue={comment}
                                onChange={handleCommentChange} multiline rows={4} fullWidth inputProps={{ maxLength: 500 }}
                                style={{ marginTop: "1rem" }} />
                            <Button variant="contained" size="small" color="primary" disabled={!comment} onClick={addNewComment}>Add Comment</Button>
                        </div>
                    }
                </CardContent>
                <CardActions>
                    <Button size="small" style={{ marginLeft: "auto" }} variant="outlined" onClick={props.handleClose}>Close</Button>
                </CardActions>
            </Card>
        </Dialog>
    )
}

export default connect()(Comments);